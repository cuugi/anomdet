#!/usr/bin/perl
#
# Description: ITKST42 Assigment2 - main
#
# Copyright (c) 2012 Jukka Loukkaanhuhta
#
#
# Commands:
# > ./kdd.pl k-fold k=10

# Defaults parameters
my %params = (
	      'k' => 10,
	      'outdir' => 'out/',
	      'input' => 'kddcup.data_10_percent',
	      'output' => 'part',
	      'arff' => 'arff'
);

my %datatypes = (
		 'duration' => 'numeric',
		 'src_bytes' => 'numeric',
		 'dst_bytes' => 'numeric',
		 'wrong_fragment' => 'numeric',
		 'hot' => 'numeric',
		 'num_failed_logins' => 'numeric',
		 'num_compromised' => 'numeric',
		 'num_root' => 'numeric',
		 'num_file_creations' => 'numeric',
		 'num_shells' => 'numeric',
		 'num_access_files' => 'numeric',
		 'num_outbound_cmds' => 'numeric',
		 'count' => 'numeric',
		 'serror_rate' => 'numeric',
		 'rerror_rate' => 'numeric',
		 'same_srv_rate' => 'numeric',
		 'diff_srv_rate' => 'numeric',
		 'srv_count' => 'numeric',
		 'srv_serror_rate' => 'numeric',
		 'srv_rerror_rate' => 'numeric',
		 'srv_diff_host_rate' => 'numeric',
		 'urgent' => 'numeric',
		 'label' => '{back,buffer_overflow,ftp_write,guess_passwd,imap,ipsweep,land,loadmodule,multihop,neptune,nmap,normal,perl,phf,pod,portsweep,rootkit,satan,smurf,spy,teardrop,warezclient,warezmaster}',
);
# Helper for getting all possible values:
# > awk -F"," '{print $3}' kddcup.data_10_percent|sort|uniq|tr \\n ','
# > awk 'max=="" || $2 > max {max=$2} END {print max}' FS="," out/part0

# Arguments
my $action = $ARGV[0];
while (@ARGV) {
  if ($ARGV[0] =~ /^(.*)=(.*)$/) {
    $params{$1} = $2;
  }
  shift @ARGV;
}
die("No action") if (!$action);

# Subs
sub ksplit {
  my $input = shift;
  my $head = shift;
  my $k = shift;
  my $p = shift;
  my $linecount = shift;

  my $outputfile = $params{'outdir'} . $params{'output'} . $p;
  die ("Could not open output file for writing: $outputfile")
    if (!open(OUTPUT, ">$outputfile"));

  # Datatypes
  my $keyi = 0;
  my %skipped;
  my $first = 1;
  foreach (split (/,/, $head)) {
    chomp;
    s/: s//;
    my $key = $_;
    my $datatype = $datatypes{$key};
    if ($datatype) {
      if ($first) {
        $first = 0;
      } else {
        print OUTPUT ",";
      }
      print OUTPUT $key;
    } else {
      $skipped{$keyi} = $key;
      print "** skipped $key\n";
    }
    $keyi++;
  }
  print OUTPUT "\n";

  my $i = $linecount / $k;
  print "** $outputfile - $i\n";
  while (<$input>) {
    # Data
    while (<INPUT>) {
      chomp; chop; # Remove whitespaces _and_ .
      my @values = split /,/;
      my $first = 1;
      for (my $i = 0; $i < $keyi; $i++) {
        if (not exists($skipped{$i})) {
          if ($first) {
	    $first = 0;
	  } else {
	    print OUTPUT ",";
	  }
          $val = $values[$i];
          $val = "anomaly" if ($i == $keyi - 1 && $val !~ /normal/);
	  print OUTPUT $val;
        }
      }
      print OUTPUT "\n";
      close OUTPUT && return if ($i-- <= 0);
    }
  }
  close OUTPUT;
}

sub kfold {
  my $k = shift;

  my $inputfile = $params{'input'};
  my $n = 0;
  $n = $1 if (`wc -l $inputfile` =~ /^(\d+)\s/);
  print "*** sub k-fold: k=$k, n=$n\n";

  mkdir $params{'outdir'};
  die("Could not open input file for reading: $inputfile")
    if (!open(INPUT, $inputfile));
  my $head = <INPUT>;
  for (my $i = 0; $i < $k; $i++) {
    ksplit(INPUT, $head, $k, $i, $n);
  }
  close INPUT;
}

# Run action
kfold($params{'k'}) if ($action =~ "k-fold");
