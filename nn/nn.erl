%
% Description: Creates a nearest neighbour graph
%
% Copyright (c) 2012 Jukka Loukkaanhuhta
%

-module(nn).

-export([info/0,
         median/2,
         read_kddfile/1,
         kdtree/1,
         nnsearch/2,
         kdd_nnsearch/2,
         kdd_test1/4,
         kdd_test2/4
        ]).

-define(INFO, <<"Nearest neighbour stuff.">>).

info() ->
    ?INFO.

%%% Read file
do_read(Lines, _, 0) ->
    lists:reverse(Lines);
do_read(Lines, File, N) ->
    case file:read_line(File) of
       {ok, Line} ->
           do_read([Line|Lines], File, N - 1);
       eof ->
           do_read(Lines, File, 0);
       {error, Error} ->
           {error, Error}
    end.
read_kddfile(Filename) ->
    {ok, File} = file:open(Filename,
        [raw, read, read_ahead]),
    Lines = do_read([], File, 1000001), % Max 1000001 lines
    file:close(File),
    MakeN = fun(Str) -> 
                case string:to_float(Str) of
                    {error, _} -> 
                      case string:to_integer(Str) of
                        {error, _} -> list_to_atom(Str);
                        {Int, _} -> Int
                      end;
                    {Number, _} -> Number
                end
            end,
    lists:map(fun(Line) -> 
                  list_to_tuple(
                    lists:map(MakeN, 
                      string:tokens(
                        string:strip(Line, both, $\n), 
                        ",")))
              end, Lines).

%%% Calculate median for key
median(N, List) ->
    SortedList = lists:keysort(N, List),
    Index = trunc(length(SortedList) / 2),
    {Index, lists:nth(Index, SortedList)}.

%%% Balanced k-d tree impl, Points is a list of Tuples
kdtree([Header|Points]) ->
    kdtree(Points, 0, tuple_size(Header) - 1). % -1 because last is always label
kdtree([], _, _) -> nil;
kdtree(Points, Depth, Dimensions) ->
    Axis = Depth rem Dimensions + 1,
    SortedPoints = lists:keysort(Axis, Points),
    MedianIndex = trunc(length(SortedPoints) / 2),
    {L1, [Node|L2]} = lists:split(MedianIndex, SortedPoints),
    {Node, 
     kdtree(L1, Depth + 1, Dimensions),
     kdtree(L2, Depth + 1, Dimensions)}.

%%% Nearest Neighbour search for k-d tree
nnsearch(Point, Tree) ->
    Dist = fun(P, N) ->
               lists:foldl(fun(X, Sum) -> X + Sum end,
                           0,
                           lists:zipwith(
                               fun(X, Y) ->
                                 if is_number(X) and is_number(Y)
                                   -> 
                                     Z = X - Y,
                                     if not (Z == 0) ->
                                       Zl = math:log(abs(Z) + 1), % +1 because log
                                       Zl * Zl;
                                     true -> 0
                                   end;
                                   true -> 0 % non-numeric values do not affect dist
                                 end
                               end,
                               tuple_to_list(P),
                               tuple_to_list(N)))
           end,
    {_, DistList} = nnsearch(Dist, Point, 0, 
                             tuple_size(Point) - 1, % -1 because
                             Tree),
    lists:keysort(1, DistList).

nnsearch(_, _, _, _, nil) -> {nil, []};
nnsearch(_, _, _, _, []) -> {nil, []};
nnsearch(DistFun, Point, Depth, Dimensions, Tree) ->
    {Node, Left, Right} = Tree,

    NodeDist = DistFun(Point, Node),
    Axis = Depth rem Dimensions + 1,
    PAxisVal = lists:nth(Axis, tuple_to_list(Point)),
    NAxisVal = lists:nth(Axis, tuple_to_list(Node)),

    {Branch, {BestDist, DistList}} = 
               if PAxisVal < NAxisVal ->
                      % io:fwrite("left (~B/~B)~n", [Axis, Dimensions]),
                      {left,nnsearch(DistFun, Point, Depth + 1, Dimensions, Left)};
                  true ->
                      % io:fwrite("right (~B/~B)~n", [Axis, Dimensions]),
                      {right,nnsearch(DistFun, Point, Depth + 1, Dimensions, Right)}
               end,
    if BestDist == nil ->
           NewBest = NodeDist;
       NodeDist < BestDist -> 
           NewBest = NodeDist;
       true -> 
           NewBest = BestDist
    end,

    AxisDist = DistFun({PAxisVal}, {NAxisVal}),
    {BranchBest, BranchList} =
    if AxisDist < NewBest ->
           % io:fwrite("-> "),
           case Branch of
               left -> 
                   nnsearch(DistFun, Point, Depth + 1, Dimensions, Right);
               right -> 
                   nnsearch(DistFun, Point, Depth + 1, Dimensions, Left)
           end;
       true ->
           {nil, []}
    end,
    if BranchBest == nil ->
           RetBest = NewBest;
       BranchBest < NewBest ->
           RetBest = BranchBest;
       true ->
           RetBest = NewBest
    end,

    {RetBest, lists:append([{NodeDist, Node}|DistList], BranchList)}.
    

%%% Tests
label3(ResultList) ->
    LabelList = 
      lists:map(
        fun({_,X}) ->
          lists:last(tuple_to_list(X))
        end, ResultList),
    [NN1,NN2,NN3|_] = LabelList,
    if NN2 == NN3 -> NN2;
      true -> NN1
    end.

runtests(Tree, TestData, NegativeLabel, PositiveLabel) ->
    Results = lists:foldl(
      fun(P, Sum) ->
        {Tested, TN, TP, FN, FP} = Sum,
        PLabel = lists:last(tuple_to_list(P)),
        ResultList = nnsearch(P, Tree),
        NNLabel = label3(ResultList),
        case NNLabel of
          PositiveLabel when PLabel == NNLabel ->
            {Tested + 1, TN, TP + 1, FN, FP};
          NegativeLabel when PLabel == NNLabel ->
            {Tested + 1, TN + 1, TP, FN, FP};
          PositiveLabel ->
            {Tested + 1, TN, TP, FN, FP + 1};
          NegativeLabel ->
            {Tested + 1, TN, TP, FN + 1, FP};
          _ -> {Tested + 1, TN, TP, FN, FP}
        end
      end, {0, 0, 0, 0, 0}, TestData),

    io:fwrite("~nPerformance metrics:~n",[]),
    {Total, TN, TP, FN, FP} = Results,
    io:fwrite("  Raw: Total=~B, TN=~B, TP=~B, FN=~B, FP~B\n", [Total, TN, TP, FN, FP]),
    Recall = TP/(TP+FN),
    Precision = TP/(TP+FP),
    FMeasure = 2/(1/Precision+1/Recall),
    io:fwrite("  False Positive Rate: ~f~n", [FP/(TN+FP)]),
    io:fwrite("  True Negative Rate: ~f~n", [TN/(TN+FP)]),
    io:fwrite("  Accuracy: ~f~n", [(TN+TP)/Total]),
    io:fwrite("  Recall: ~f~n", [Recall]),
    io:fwrite("  Precision: ~f~n", [Precision]),
    io:fwrite("  F-measure: ~f~n~n", [FMeasure]),
    Results.

% nn:kdd_kdtree("testdata.txt").
kdd_kdtree(Filename) ->
    Contents = read_kddfile(Filename),
    kdtree(Contents).
kdd_kdtree_many(Filenames) ->
    kdtree(
      lists:umerge(
        lists:map(
          fun(Filename) -> 
            read_kddfile(Filename) 
          end, Filenames))).

% nn:kdd_nnsearch("diabetes.txt",{8,147,81,0,0,29,0.178,52,not_tested}).
% nn:kdd_nnsearch("part0",
%   {0,239,1521,0,0,0,0,0,0,0,0,0,0,3,42,0.0,0.0,0.0,0.0,1.01,0.0,0.1,normal}).
% nn:kdd_nnsearch("part0",
%   {0,240,1521,0,0,0,0,0,0,0,0,0,0,3,42,0.0,0.0,0.0,0.0,1.0,0.0,0.11,normal}).
kdd_nnsearch(Filename, Point) ->
    io:fwrite("Creating a balanced k-d tree from: ~s~n", [Filename]),
    Tree = kdd_kdtree(Filename),
    io:fwrite("Searching...~n", []),
    [NN|_] = nnsearch(Point, Tree),
    NN.

% nn:kdd_test1("testdata_train.txt", "testdata_test.txt", tested_negative, tested_positive).
% nn:kdd_test1("train", "test", normal, anomaly).
kdd_test1(Train, Test, NegativeLabel, PositiveLabel) ->
    io:fwrite("Creating a balanced k-d tree: ~s~n", [Train]),
    Tree = kdd_kdtree(Train),

    io:fwrite("Reading test data: ~s~n", [Test]),
    TestData = read_kddfile(Test),

    io:fwrite("Running tests: ~B~n", [length(TestData)]),
    runtests(Tree, TestData, NegativeLabel, PositiveLabel).

% nn:kdd_test2(["part1","part2","part3","part4","part5","part6","part7","part8", "part9"], "part0", normal, anomaly).
kdd_test2(Trains, Test, NegativeLabel, PositiveLabel) ->
    io:fwrite("Creating a balanced k-d tree: ~s~n", [Trains]),
    Tree = kdd_kdtree_many(Trains),

    io:fwrite("Reading test data: ~s~n", [Test]),
    TestData = read_kddfile(Test),

    io:fwrite("Running tests: ~B~n", [length(TestData)]),
    runtests(Tree, TestData, NegativeLabel, PositiveLabel).
